/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.workflow.util;

/**
 *
 * @author PPAGOTTO <pedropagotto@outlook.com>
 */

/*classe para receber os normes das tabelas que tem que ser limpas no metodo persist*/
public class Tabelas {
    private String nome_tabela;

    public String getNome_tabela() {
        return nome_tabela;
    }

    public void setNome_tabela(String nome_tabela) {
        this.nome_tabela = nome_tabela;
    }
    
}
